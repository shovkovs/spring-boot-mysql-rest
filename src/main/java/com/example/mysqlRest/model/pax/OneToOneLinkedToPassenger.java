package com.example.mysqlRest.model.pax;

import com.example.mysqlRest.model.AbstractModel;

import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

@MappedSuperclass
public abstract class OneToOneLinkedToPassenger extends AbstractModel implements LinkedToPassenger {
	@OneToOne
	@JoinColumn(name = "passenger_id", nullable = false, unique = true)
	protected Passenger passenger;

	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}
}
